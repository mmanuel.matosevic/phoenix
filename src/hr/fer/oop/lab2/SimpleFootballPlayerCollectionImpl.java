package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.SimpleFootballPlayerCollection;

public class SimpleFootballPlayerCollectionImpl implements SimpleFootballPlayerCollection {

    private FootballPlayer[] players;

    public SimpleFootballPlayerCollectionImpl(int size){
        players = new FootballPlayer[size];
    }

    @Override
    public boolean add(FootballPlayer player) {
        if(!contains(player)){
            for (FootballPlayer p : players){
                if(p == null){
                    p = player;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int calculateEmotionSum() {
        int emotionSum = 0;
        for(FootballPlayer player : players){
            if(player != null){
                emotionSum += player.getEmotion();
            }
        }
        return  emotionSum;
    }

    @Override
    public int calculateSkillSum() {
        int skillSum = 0;
        for(FootballPlayer player : players){
            if(player != null){
                skillSum += player.getPlayingSkill();
            }
        }
        return skillSum;
    }

    @Override
    public void clear() {
        for(FootballPlayer p : players){
            p = null;
        }
    }

    @Override
    public boolean contains(FootballPlayer player) {
        if(player == null) return false;
        for (FootballPlayer p : players){
            if (p != null){
                if(p.equals(player)) return true;
            }
        }
        return false;
    }

    @Override
    public int getMaxSize() {
        return players.length;
    }

    @Override
    public FootballPlayer[] getPlayers() {
        return players;
    }

    @Override
    public int size() {
        int size = 0;
        for(FootballPlayer player : players){
            if (player != null) size++;
        }
        return size;
    }
}
