package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.Constants;
import hr.fer.oop.lab2.welcomepack.Formation;
import hr.fer.oop.lab2.welcomepack.ManageableTeam;
import hr.fer.oop.lab2.welcomepack.SimpleFootballPlayerCollection;

public abstract class Team implements ManageableTeam{

    private String name;
    private Formation formation;
    private SimpleFootballPlayerCollection registeredPlayers;
    private SimpleFootballPlayerCollection startingEleven;

    public Team(int registeredPlayersSize){
        name = Constants.DEFAULT_TEAM_NAME;
        formation = Constants.DEFAULT_FORMATION;
        registeredPlayers = new SimpleFootballPlayerCollectionImpl(registeredPlayersSize);
        startingEleven = new SimpleFootballPlayerCollectionImpl(Constants.STARTING_ELEVEN_SIZE);

    }

    public Team(String name, Formation formation, int registeredPlayersSize) {
        this.name = name;
        setFormation(formation);
        this.registeredPlayers = new SimpleFootballPlayerCollectionImpl(registeredPlayersSize);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegisteredPlayers(SimpleFootballPlayerCollection registeredPlayers) {
        this.registeredPlayers = registeredPlayers;
    }

    public void setStartingEleven(SimpleFootballPlayerCollection startingEleven) {
        this.startingEleven = startingEleven;
    }


    @Override
    public boolean addPlayerToStartingEleven(FootballPlayer player){
        if(registeredPlayers.contains(player) && !startingEleven.contains(player) && startingEleven.size()<Constants.STARTING_ELEVEN_SIZE){
            startingEleven.add(player);
            return true;
        }
        return false;
    }

    @Override
    public boolean isPlayerRegistered(FootballPlayer player){
        return registeredPlayers.contains(player);
    }

    @Override
    public SimpleFootballPlayerCollection getRegisteredPlayers(){
        return registeredPlayers;
    }

    public SimpleFootballPlayerCollection getStartingEleven(){
        return startingEleven;
    }

    @Override
    public void clearStartingEleven() {
        startingEleven.clear();
    }

    @Override
    public void setFormation(Formation formation) {
        if(formation!=null)
            this.formation = formation;
        else System.err.println("Formacija je null! :(");
    }

    @Override
    public Formation getFormation() {
        return formation;
    }




}
