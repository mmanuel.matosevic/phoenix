package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.Constants;

import java.util.Objects;

public abstract class Person {

    private final String name;
    private final String country;
    private int emotion;

    public Person(){
        this.name = Constants.DEFAULT_PLAYER_NAME;
        this.country = Constants.DEFAULT_COUNTRY;
        this.emotion = Constants.DEFAULT_EMOTION;
    }

    public Person(String name, String country, int emotion){
        this.name = name;
        this.country = country;
        this.emotion = emotion;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public int getEmotion() {
        return emotion;
    }

    public void setEmotion(int emotion) {
        this.emotion = emotion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return getName().equals(person.getName()) &&
                getCountry().equals(person.getCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCountry());
    }
}
