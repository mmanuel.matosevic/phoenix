package hr.fer.oop.lab2;


import hr.fer.oop.lab2.welcomepack.Constants;
import hr.fer.oop.lab2.welcomepack.Formation;

public class ClubTeam extends Team{

    private int reputation;

    public ClubTeam(){
        super(Constants.MAX_NO_PLAYERS_CLUB);
        this.reputation = Constants.DEFAULT_REPUTATION;
    }

    public ClubTeam(String name, Formation formation, int reputation){
        super(name, formation, Constants.MAX_NO_PLAYERS_CLUB);
        this.reputation = reputation;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    @Override
    public boolean registerPlayer(FootballPlayer player) {
        if(player == null) return false;
        if(getReputation() > player.getPlayingSkill() || getRegisteredPlayers().size() >= getRegisteredPlayers().getMaxSize()) return false;
        getRegisteredPlayers().add(player);
        return true;
    }

    @Override
    public double calculateRating() {
        return Constants.THIRTY_PERCENT * getRegisteredPlayers().calculateEmotionSum() + Constants.SEVENTY_PERCENT * getRegisteredPlayers().calculateSkillSum();
    }
}
