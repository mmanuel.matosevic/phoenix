package hr.fer.oop.lab1.third;

import java.util.Scanner;

public class PrimeNumbers {
    public static void main(String[] args){
        System.out.println("Prime numbers: ");
        Scanner sc = new Scanner(System.in);
        int primes = sc.nextInt();
        sc.close();

        System.out.printf("You requested calculation of first %d prime numbers. Here they are: \n", primes);

        int counter = 0;
        int number = 2;
        do{
            if(isPrime(number)){
                counter++;
                System.out.println(counter + ". " + number);
            }
            number++;
        }
        while(counter<primes);
    }

    private static boolean isPrime(int n){
        if(n == 2) return true;
        else if(n % 2 == 0) return false;
        else{
            for(int i=3; i<=Math.sqrt(n); i++){
                if(n % i == 0){
                    return false;
                }
            }
        }
        return true;
    }
}
