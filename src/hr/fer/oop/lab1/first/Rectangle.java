package hr.fer.oop.lab1.first;

import java.util.Scanner;

//Simple app to calculate area and perimeter of a Rectangle

public class Rectangle {

    public static void main(String[] args){
        double width;
        double height;
        double area;
        double perimeter;

        if(args.length == 0){
            Scanner sc = new Scanner(System.in);
            width = getInput(sc, "width");
            height = getInput(sc, "height");
            area = calculateArea(width,height);
            perimeter = calculatePerimeter(width, height);
            sc.close();
        }
        else if(args.length == 2){
            width = Double.parseDouble(args[0]);
            height = Double.parseDouble(args[1]);
            area = calculateArea(width, height);
            perimeter = calculatePerimeter(width, height);

        }
        else{
            System.err.println("Invalid number of arguments was provided");
            return;
        }

        System.out.println("You have specified a rectangle of width " + width + " and height " + height + ". " +
                "It's area is " + area + " and its perimeter is " + perimeter);

    }

    public static double getInput(Scanner sc, String argument){
        while(true){
            System.out.println("Please provide " + argument + ":");
            double value = sc.nextDouble();
            if(value <= 0) System.out.println("The width must not be negative");
            else return value;
        }
    }

    public static double calculateArea(double width, double height){
        return width * height;
    }

    public static double calculatePerimeter(double width, double height){
        return 2*width + 2*height;
    }


}
