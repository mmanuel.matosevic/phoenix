package hr.fer.oop.lab1.second;

import java.util.Scanner;

public class Roots {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        double real = sc.nextDouble();
        double imag = sc.nextDouble();
        double root = sc.nextDouble();

        double r = calculateAbs(real, imag);
        double fi = calculateFi(real, imag);

        System.out.println("You requested calculation of " + root + ". roots. Solutions are:");
        for(int i=0; i<root; i++){
            double re = Math.pow(r, 1/root) * Math.cos((fi+2*i*Math.PI)/root);
            double im = Math.pow(r, 1/root) * Math.sin((fi+2*i*Math.PI)/root);
            System.out.println((i+1) + ")" + re + (im>0 ? " + " : " ") + im);
        }

    }

    public static double calculateAbs(double real, double imag){
        double r = Math.sqrt(Math.pow(real, 2) + Math.pow(imag, 2));
        return r;
    }

    public static double calculateFi(double real, double imag){
        double fi = Math.atan(imag/real);
        return fi;
    }
}
